#Андреев Андрей.
class Aircraft:
    def __init__(self, name: str, year: int, wing_shape: str, flight_setup = None):
        self.name = name
        self.year = year
        self.wing_shape = wing_shape
        self.flight_setup = flight_setup
        
    def flight(self,params):
        flight_setup = params
        print("Flight Mode activated")
    
    def takeoff_landing(self, params):
        flight_setup = params
        print("Takeoff/Landing Mode activated")
    
    def print_info(self):
        print(self.name, "\n", "Year:", self.year, "\n", "Wing shape:", self.wing_shape)

Cesna_172 = Aircraft("Cesna 172", 1956, "straight wing")
Cesna_172.print_info()
Cesna_172.flight("params")
Cesna_172.takeoff_landing("params")
