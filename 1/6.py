#Андреев Андрей.На вход функции my_func() подается список целых чисел. Написать функцию date, принимающую 3 аргумента — день, месяц и год. Вернуть True, если такая дата есть в нашем календаре, и False иначе.
import datetime
import re
def my_func(datelist):
    try:
        datetime.date(datelist[0], datelist[1], datelist[2])
        return True
    except ValueError:
        return False
entered_list = input().split()
print(my_func(list(map(int, entered_list))))
