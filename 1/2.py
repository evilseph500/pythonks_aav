#Андреев Андрей. Проверить является ли входная строка палиндромомом. 
def is_palindrome(string):
    return(True if string.lower() == string.lower()[::-1] else False)
print(is_palindrome(input()))
