#Андреев Андрей. 1. Декоратор считающий количество вызовов 
def dec_counter(func):
    
    def wrapper(*args, **kwargs):
        wrapper.count +=1
        return func(*args, **kwargs)
    wrapper.count = 0
    return wrapper

@dec_counter
def f():
    pass
f()
f()
f()
f()
print(f.count)
