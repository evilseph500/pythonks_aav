#Андреев Андрей.
class Aircraft:
    def __init__(self, name: str, year: int, wing_shape: str, flight_setup = None):
        self.name = name
        self.year = year
        self.wing_shape = wing_shape
        self.flight_setup = flight_setup
        
    def flight(self,params):
        flight_setup = params
        print("Flight Mode activated")
    
    def takeoff_landing(self, params):
        flight_setup = params
        print("Takeoff/Landing Mode activated")
    
    def print_info(self):
        print(self.name, "\n", "Year:", self.year, "\n", "Wing shape:", self.wing_shape)

class SupersonicAircraft(Aircraft):
    def __init__(self, name: str, year: int, wing_shape: str, engine_type:str, flight_setup = None):
        super().__init__(name, year, wing_shape)
        self.engine_type = engine_type
    
    def supersonic_fligth(self,params):
        fligth_setup = params
        print("Supersonic Mode activated")
    
    def print_info(self):
        print(self.name, "\n", "Year:", self.year, "\n", "Wing shape:", self.wing_shape, "\n", "Engine type:",self.engine_type)

Cesna_172 = Aircraft("Cesna 172", 1956, "straight wing")
Cesna_172.print_info()
Cesna_172.flight("params")
Cesna_172.takeoff_landing("params")

SR_71 = SupersonicAircraft("Lockheed SR-71", 1966, "ogival wing", "P&W JP58-P4 Scramjet", )
SR_71.print_info()
SR_71.supersonic_fligth("params")
SR_71.takeoff_landing("params")
