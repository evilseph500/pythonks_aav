#Андреев Андрей.На вход функции my_func() подается список целых чисел. На выходе выполнения функци получить кортеж уникальных элементов списка в обратном порядке.
def my_func(listA):
    listA = set(listA)
    listA = tuple(listA)
    return(listA)
list0 = [c for c in range(0, 10)]
list1 = [c for c in range(5,15)]
list0+=list1
print("list:",list0)
print(my_func(list0))
